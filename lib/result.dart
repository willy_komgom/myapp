import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result({this.resultScore, this.resetHandler});

  String get resultPhrase {
    var resultText = 'You did it!';

    if (resultScore <= 20) {
      resultText = 'You are awesome and innocent!';
    } else if (resultScore <= 25) {
      resultText = 'Pretty likeable!';
    } else if (resultScore <= 35) {
      resultText = 'You are ... strange?!';
    } else {
      resultText = 'You are so bad!';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    var resetHandler2 = resetHandler;
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text('Restart Quiz'),
            onPressed: resetHandler2,
            textColor: Colors.cyan,
          ),
        ],
      ),
    );
  }
}
