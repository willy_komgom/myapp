import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/result.dart';

import './quiz.dart';
import 'result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIdx = 0;
  var _totalScore = 0;
  final _questions = [
    {
      'questionText': 'What\'s your favorite color?',
      'answers': [{'text': 'Black','score' : 10}, {'text': 'Red', 'score': 5}, {'text' : 'Green', 'score': 3},  {'text' : 'White', 'score': 1}],
    },
    {
      'questionText': 'What\'s your favorite animal?',
      'answers': [{'text': 'Rabbit','score' : 10}, {'text': 'Snake', 'score': 25}, {'text' : 'Elephant', 'score': 15},  {'text' : 'Lion', 'score': 5}],
    },
    {
      'questionText': 'Who\'s your favorite instructor?',
      'answers': [{'text': 'Max','score' : 10}, {'text': 'Willy', 'score': 5}, {'text' : 'Georges', 'score': 3},  {'text' : 'Portilla', 'score': 15}],
    },
  ];
  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIdx += 1;
    });
    print(_questionIdx);
    if (_questionIdx < _questions.length) {
      print('We have more questions');
    }
  }

  void _resetQuiz() {
    setState(() {
      _totalScore = 0;
      _questionIdx= 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My first App'),
        ),
        body: _questionIdx < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIdx: _questionIdx,
                questions: _questions,
              )
            : Result(resultScore: _totalScore, resetHandler: _resetQuiz),
      ),
    );
  }
}
